<?php
include('common/login.php');
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MAGITEK INTERNACIONAL S.A.S</title>
	<link rel="stylesheet" href="../css/mainlogin.css">

</head>
<body>
<div class="container">
        <div class="card card-container">
            <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
            <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
            <p id="profile-name" class="profile-name-card"></p>
            <form class="form-signin" action="#" method="post">
                <span id="reauth-email" class="reauth-email"></span>
                <input type="email" id="inputEmail" name="inputEmail" class="form-control" placeholder="Usuario" required autofocus>
                <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Contraseña" required>
                <div id="remember" class="checkbox">
                    <label>
                        <span><?php echo $error; ?></span><br>
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit" name="enviar">Iniciar sesión </button>
            </form><!-- /form -->
            <a href="register_user.php" class="forgot-password">
                Registrarse
            </a>
        </div><!-- /card-container -->
    </div>
  <script type="text/javascript" src="./main.js"></script>
</body>
</html>