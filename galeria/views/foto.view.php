<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no,
	 initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
	<script src="https://use.fontawesome.com/a37ff62a1b.js"></script>
	<link rel="stylesheet" href="css/estilos.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<title>Galeria</title>

</head>
<body>
	<header>
		<div class="container">
			<h1 class="titulo">Foto: <?php if (!empty($foto['titulo'])) {
				echo $foto['titulo'];
			} else {
				echo $foto['imagen'];
			} ?></h1>
		</div>
	</header>
	<section class="container">
		<div class="row">
			<img src="img/<?php echo $foto['imagen']; ?>" alt="">
			<p class="texto"><?php echo $foto['texto']; ?> </p>
			<a href="index.php" class="regresar"><i class="fa fa-long-arrow-left"></i> Regresar</a>

		<!--<div class="paginacion">	
				<a href="#" class="izquierda"><i class="fa fa-long-arrow-left">	</i> Pagina Anterior</a>
				<a href="#" class="derecha">Pagina Siguiente <i class="fa fa-long-arrow-right">	</i></a>
				</div>-->
	</section>

	<footer>	
	
	<p class="copyright">	Galeria creada por Alfredo Duran	</p>

	</footer>
</body>
</html>