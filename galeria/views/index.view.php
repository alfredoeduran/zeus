<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no,
	 initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
	<script src="https://use.fontawesome.com/a37ff62a1b.js"></script>
	<link rel="stylesheet" href="css/estilos.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<title>Galeria</title>

</head>
<body>
<header>
	<div class="container">
		<h1 class="text-center">Mi increible galeria en php y mysql</h1>
		</div>
</header>
<section class="contenedor">
	<div class="">
		<?php foreach($fotos as $foto):?>
			<div class="col-sm-3">
				<a href="foto.php?id=<?php echo $foto['id']; ?>">
					<img src="img/<?php echo $foto['imagen'] ?>" alt="">
				</a>
			</div>
		<?php endforeach;?>

		<div class="paginacion">	
			<?php if ($pagina_actual > 1): ?>
				<a href="index.php?p=<?php echo $pagina_actual - 1; ?>" class="izquierda"><i class="fa fa-long-arrow-left">	</i> Pagina Anterior</a>
			<?php endif ?>
				
			<?php if ($total_paginas != $pagina_actual): ?>
				<a href="index.php?p=<?php echo $pagina_actual + 1; ?>" class="derecha">Pagina Siguiente <i class="fa fa-long-arrow-right">	</i></a>
			<?php endif ?>
		</div>
	</div>
</section>

<footer>	
	<p class="copyright">	Zeus Cucine 2017 &copy	</p>
</footer>

</body>

</html>