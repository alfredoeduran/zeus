<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no,
	 initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
	<script src="https://use.fontawesome.com/a37ff62a1b.js"></script>
	<link rel="stylesheet" href="css/estilos.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<title>Galeria</title>

</head>
<body>
	<header>
		<div class="container">
			<h1 class="titulo">subir foto</h1>
		</div>
	</header>
	<section class="container">
		<form class="formulario" method="post" enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
			
			<label for="foto">Selecciona tu foto</label>
			<input type="file" id="foto" name="foto">

			<label for="titulo">Titulo de la foto</label>
			<input type="text" id="titulo" name="titulo">

			<label for="texto">Descripcion</label>
			<textarea name="texto" id="texto" placeholder="Ingresa una Descripcion"></textarea>
			
			
			<input type="submit" class="submit" value="Subir Foto">
		</form>
	</section>

	<footer>	
	
	<p class="copyright">	Galeria creada por Alfredo Duran	</p>

	</footer>
</body>
</html>